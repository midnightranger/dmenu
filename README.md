This is my personal build of 'dmenu', from suckless.org Team. The patches I have applied are the below:

1. border: This patch adds a border around the dmenu window. It is intended to be used with the center or xyw patches, to make the menu stand out from similarly coloured windows.
2. case-insensitive: This patch changes case-insensitive item matching to default behaviour. Adds an -s option to enable case-sensitive matching.
3. center: This patch centers dmenu in the middle of the screen.
4. grid: This patch allows you to render dmenu's entries in a grid by adding a new -g flag to specify the number of grid columns. You can use -g and -l together to create a G columns * L lines grid.
5. xresources: This patch adds the ability to configure dmenu via Xresources.
6. gridnav: This patch adds the ability to move left and right through a grid. Apply this patch after grid.
7. scroll: This patch adds support for text scrolling
8. xyx: The patch adds options for setting window position and width. (for the 5.0 version, the width parameter is '-z' to avoid conflict with '-w windowid'.)
